#!/bin/bash

#OAIRANDIR="/local/openairinterface5g-develop"
#ENBEXE="lte-softmodem.Rel15"

OAIRANDIR="/local/openairinterface5g"
ENBEXE="lte-softmodem.Rel14"

ENBEXEPATH="$OAIRANDIR/targets/bin/$ENBEXE"
ENBCONFPATH="/local/repository/etc/enb.conf"

sudo $ENBEXEPATH -O $ENBCONFPATH  2>&1 | tee /local/repository/bin/run_enb_out.txt
