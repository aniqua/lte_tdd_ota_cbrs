#!/usr/bin/python

"""
Experimental profile based on the ota-cbrs profile to test OTA LTE-TDD. The profile has
options to request the allocation of SDR radios in rooftop base-stations as well as a compute node to use as the EPC.
"""

# Library imports
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as PN
import geni.rspec.emulab.spectrum as spectrum
import geni.rspec.igext as ig
import geni.urn as URN


# Global Variables
image = "urn:publicid:IDN+emulab.net+image+PowderTeam:U18-GR-PBUF"

nuc_image = image

srsLTE_image = "urn:publicid:IDN+emulab.net+image+PowderTeam:U18LL-SRSLTE:4"
srsLTE_src_ds = "urn:publicid:IDN+emulab.net:powderteam+imdataset+srslte-src-v19"

oai_eNB_image = image
#oai_eNB_image = "urn:publicid:IDN+emulab.net+image+PowderAlexOffice:oaidev1804lowlatency"
#oai_eNB_image = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:OAI-Real-Hardware.enb1")
#oai_ds = "urn:publicid:IDN+emulab.net:phantomnet+ltdataset+oai-develop"
#oai_eNB_image ="urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"


   

# Top-level request object.
request = portal.context.makeRequestRSpec()

# Helper function that allocates a PC + X310 radio pair, with Ethernet
# link between them.
def x310_node_pair(idx, x310_radio_name, node_type, role):
    radio_link = request.Link("radio-link-%d" % idx)

    node = request.RawPC("%s-%s" % (x310_radio_name,role))
    node.hardware_type = node_type

    if role.startswith('ue'):
        node.disk_image = srsLTE_image
        bs = node.Blockstore("bs-comp-%s"%idx, "/opt/srslte")
        bs.dataset = srsLTE_src_ds
    elif role == 'enb':
        node.disk_image = oai_eNB_image
    else:
        print('Unrecognized role')
        exit(1)

    node_radio_if = node.addInterface("usrp_if")
    node_radio_if.addAddress(rspec.IPv4Address("192.168.40.1",
                                               "255.255.255.0"))
    radio_link.addInterface(node_radio_if)

    radio = request.RawPC("%s-x310" % x310_radio_name)
    radio.component_id = x310_radio_name
    radio_link.addNode(radio)


# List of CBRS rooftop X310 radios.
rooftop_names = [
    ("cbrssdr1-bes",
     "Behavioral"),
    ("cbrssdr1-browning",
     "Browning"),
    ("cbrssdr1-dentistry",
     "Dentistry"),
    ("cbrssdr1-fm",
     "Friendship Manor"),
    ("cbrssdr1-hospital",
     "Hospital"),
    ("cbrssdr1-honors",
     "Honors"),
    ("cbrssdr1-meb",
     "MEB"),
    ("cbrssdr1-smt",
     "SMT"),
    ("cbrssdr1-ustar",
     "USTAR"),
]

# A list of endpoint sites.
fe_sites = [
    ('urn:publicid:IDN+bookstore.powderwireless.net+authority+cm',
     "Bookstore"),
    ('urn:publicid:IDN+cpg.powderwireless.net+authority+cm',
     "Garage"),
    ('urn:publicid:IDN+ebc.powderwireless.net+authority+cm',
     "EBC"),
    ('urn:publicid:IDN+guesthouse.powderwireless.net+authority+cm',
     "GuestHouse"),
    ('urn:publicid:IDN+humanities.powderwireless.net+authority+cm',
     "Humanities"),
    ('urn:publicid:IDN+law73.powderwireless.net+authority+cm',
     "Law73"),
    ('urn:publicid:IDN+madsen.powderwireless.net+authority+cm',
     "Madsen"),
    ('urn:publicid:IDN+moran.powderwireless.net+authority+cm',
     "Moran"),
    ('urn:publicid:IDN+sagepoint.powderwireless.net+authority+cm',
     "SagePoint"),
    ('urn:publicid:IDN+web.powderwireless.net+authority+cm',
     "WEB"),
]

# Frequency/spectrum parameters
portal.context.defineStructParameter(
    "freq_ranges", "Range", [],
    multiValue=True,
    min=1,
    multiValueTitle="Frequency ranges for over-the-air operation.",
    members=[
        portal.Parameter(
            "freq_min",
            "Frequency Min",
            portal.ParameterType.BANDWIDTH,
            3550.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
        portal.Parameter(
            "freq_max",
            "Frequency Max",
            portal.ParameterType.BANDWIDTH,
            3560.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
    ])

# Node type parameter for PCs to be paired with X310 radios.
# Restricted to those that are known to work well with them.
portal.context.defineParameter(
    "nodetype",
    "Compute node type",
    portal.ParameterType.STRING, "d740",
    ["d740","d430"],
    "Type of compute node to be paired with the X310 Radios",
)

# Node type for the epc.
portal.context.defineParameter(
    "epctype",
    "EPC node type",
    portal.ParameterType.STRING, "None",
    ["None", "d430","d740"],
    "Type of compute node for the EPC (unset == 'any available')",
)

# CBRS radio for tje eNodeB
portal.context.defineParameter(
    "enb",
    "eNodeB radio",
    portal.ParameterType.STRING, rooftop_names[0],
    rooftop_names,
    "CBRS Radio to allocate as eNB",
)

# Multi-value list of x310+PC pairs to add to experiment as UEs
portal.context.defineStructParameter(
    "ue", "CBRS Radios to allocate as UEs", [],
    multiValue=True,
    min=0,
    multiValueTitle="CBRS Radios to allocate as UEs",
    members=[
        portal.Parameter(
            "radio_name",
            "Rooftop base-station X310",
            portal.ParameterType.STRING,
            rooftop_names[0],
            rooftop_names)
    ])


# Set of Fixed Endpoint devices to allocate as listerners(nuc1)
portal.context.defineStructParameter(
    "fe_radio_sites_nuc1", "Fixed Endpoint Sites", [],
    multiValue=True,
    min=0,
    multiValueTitle="Fixed Endpoint NUC1+B210 radios to allocate as listener.",
    members=[
        portal.Parameter(
            "site",
            "FE Site",
            portal.ParameterType.STRING,
            fe_sites[0], fe_sites,
            longDescription="A `nuc1` device will be selected at the site."
        ),
    ])


#portal.context.defineStructParameter(
#    "radios", "X310 CBRS Radios",
#    multiValue=False,
#    members=[
#        portal.Parameter(
#            "radio_name1",
#            "Rooftop base-station X310",
#            portal.ParameterType.STRING,
#            rooftop_names[0],
#            rooftop_names)
#    ])

# Bind and verify parameters
params = portal.context.bindParameters()

for i, frange in enumerate(params.freq_ranges):
    if frange.freq_min < 3400 or frange.freq_min > 3800 \
       or frange.freq_max < 3400 or frange.freq_max > 3800:
        perr = portal.ParameterError("Frequencies must be between 3400 and 3800 MHz", ["freq_ranges[%d].freq_min" % i, "freq_ranges[%d].freq_max" % i])
        portal.context.reportError(perr)
    if frange.freq_max - frange.freq_min < 1:
        perr = portal.ParameterError("Minimum and maximum frequencies must be separated by at least 1 MHz", ["freq_ranges[%d].freq_min" % i, "freq_ranges[%d].freq_max" % i])
        portal.context.reportError(perr)

portal.context.verifyParameters()

# Allocate EPC
if params.epctype != "None":
    epc = request.RawPC("epc")
    epc.disk_image = srsLTE_image
    epc.hardware_type = params.epctype
    bs = epc.Blockstore("bs-comp", "/opt/srslte")
    bs.dataset = srsLTE_src_ds

# Request frequency range(s)
for frange in params.freq_ranges:
    request.requestSpectrum(frange.freq_min, frange.freq_max, 100)

# Request PC + X310 resource pairs.
for i, ue in enumerate(params.ue):
	x310_node_pair(i, ue.radio_name, params.nodetype, 'ue'+str(i))

x310_node_pair(i, params.enb, params.nodetype,  'enb')

# Request nuc1+B210 radio resources at FE sites.
for fesite in params.fe_radio_sites_nuc1:
    nuc = ""
    for urn,sname in fe_sites:
        if urn == fesite.site:
            nuc = request.RawPC("%s-b210" % sname)
            break
    nuc.component_manager_id = fesite.site
    nuc.component_id = "nuc1"
    nuc.disk_image = nuc_image




# Emit!
portal.context.printRequestRSpec()
